import { useState } from "react";
import PageHeader from "../components/PageHeader";
import Memo from "../components/useMemo/Memo";

export default function UseMemo() {
  const [amount] = useState(1);

  return (
    <>
      <PageHeader
        title="useMemo.tsx"
        description="Caching values locally inside components during rerendering with useMemo/useCallback."
      />
      {Array.from(Array(amount).keys()).map((i) => (
        <Memo key={i} />
      ))}
    </>
  );
}
