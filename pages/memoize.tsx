import { useState } from "react";
import { Button } from "semantic-ui-react";
import PageHeader from "../components/PageHeader";
import PeopleMemoize from "../components/memoize/PeopleMemoize";
import getData from "../components/memoize/getData";
import memoize from "lodash.memoize";

export default function Memoize() {
  const [amount, setAmount] = useState(0);

  return (
    <>
      <PageHeader
        title="memoize.tsx"
        description="Memoize a Promise that fetches our data, with Lodash.memoize"
      />
      {Array.from(Array(amount).keys()).map((i) => (
        <PeopleMemoize key={i} />
      ))}
      <Button onClick={() => setAmount((a) => ++a)}>Add component</Button>
      <Button onClick={() => (getData.cache = new memoize.Cache())}>
        Invalidate cache
      </Button>
      <Button onClick={() => setAmount(0)}>Reset</Button>
    </>
  );
}
