import { Icon, Label, Segment } from "semantic-ui-react";
import { getColor } from "../src/colors";

export default function PeopleRenderer({ json }) {
  return (
    <Segment className="peopleSegment">
      <Label>People:</Label>
      {!json ? (
        <Label color="orange">
          <Icon loading name="spinner" />
          Fetching...
        </Label>
      ) : (
        <>
          <Label>{json.people.map((person) => person.name).join(", ")}</Label>
          <Label color={getColor(json.randomNumber)}>
            Random number from API:
            <Label.Detail>{json.randomNumber}</Label.Detail>
          </Label>
        </>
      )}
    </Segment>
  );
}
